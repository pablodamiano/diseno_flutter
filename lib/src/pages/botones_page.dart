import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
class BotonesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _botomNavigationBar(context),
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _titulos(),
                _botonesredondeados(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _fondoApp() {
    final gradiente = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset(0.0, 0.6),
          end: FractionalOffset(0.0, 1.0),
          colors:[
            Color.fromRGBO(52, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0),
          ], 
        ),
      ),
    );
   final cajarosada = Transform.rotate(
     angle: -pi/5.0,
     child:  Container(
     height: 360.0,
     width: 360.0,
     decoration: BoxDecoration(
       color: Colors.pink,
       borderRadius: BorderRadius.circular(90.0),
       gradient: LinearGradient(
         colors:[
           Color.fromRGBO(236, 98, 188, 1.0),
           Color.fromRGBO(241, 142, 172, 1.0),
         ],
       )

     ),
   ),
     
    );
   
   
  
    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(
          top: -100,
          child: cajarosada
        ),
      ],
    );
  }

  Widget _titulos() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Classify transaction',style: TextStyle(color: Colors.white,fontSize: 30.0,fontWeight: FontWeight.bold),),
            SizedBox(height:10.0),
            Text('Classify this transaction into a particular category ',style: TextStyle(color: Colors.white,fontSize: 18.0),)
          ],
        ),
      ),
    );
  }

 Widget _botomNavigationBar(BuildContext context){
   return Theme(
     data: Theme.of(context).copyWith(
       canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
       primaryColor: Colors.pinkAccent,
       textTheme:Theme.of(context).textTheme.copyWith(caption:TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))

     ), 
     child: BottomNavigationBar(
        items:<BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Container(),
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart_outlined),
            title: Container(),
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.supervised_user_circle),
            title: Container(),
          ),
        ],
      ),
   );
 }
 Widget _botonesredondeados() {
   return Table(
     children:[
       TableRow(
         children: [
           _crearbotonredondeado(Colors.blue,'General',Icons.border_all),
           _crearbotonredondeado(Colors.purpleAccent,'Bus',Icons.directions_bus),
         ]
       ),
       TableRow(
         children: [
          _crearbotonredondeado(Colors.pink,'Buy',Icons.shop),
          _crearbotonredondeado(Colors.orange,'File',Icons.insert_drive_file),
         ]
       ),
       TableRow(
         children: [
          _crearbotonredondeado(Colors.blueAccent,'Entertaiment',Icons.movie_filter),
          _crearbotonredondeado(Colors.green,'Grocery',Icons.cloud),
         ]
       ),
        TableRow(
         children: [
          _crearbotonredondeado(Colors.red,'Photos',Icons.collections),
          _crearbotonredondeado(Colors.teal,'HelpOutline',Icons.help_outline),
         ]
       ),
     ]
   );
 }

  Widget _crearbotonredondeado(Color color, String text, IconData iconData) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10.0,sigmaY: 10.0),
        child: Container(
          height: 180.0,
          margin: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Color.fromRGBO(62, 66, 107, 0.7),
            borderRadius: BorderRadius.circular(20.0),
          ),
          child:Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              CircleAvatar(
                backgroundColor:color,
                radius: 35.0,
                child: Icon(iconData,color: Colors.white,size: 20.0,),
              ),
              Text(text,style: TextStyle(color:color),)
            ],
          ),
        ),
      ),
    );
  }
}