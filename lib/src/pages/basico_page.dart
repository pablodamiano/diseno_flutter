import 'package:flutter/material.dart';
class BasicoPage extends StatelessWidget {
  final estilortitulo = TextStyle(fontSize:20.0, fontWeight: FontWeight.bold);
  final estilosubtitulo = TextStyle(fontSize:20.0,color: Colors.grey);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
            children: <Widget>[
          _crearImagen(),
          _crearTitulo(),
          _crearAcciones(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
          _crarTexto(),
        ],
        ),
      )
    );
      
    
  }

Widget _crearImagen(){
  return  Container(
    width: double.infinity,
    child: Image(
    image: NetworkImage('https://i.pinimg.com/originals/57/6d/42/576d428484ca758011f214491db385e7.jpg'),
    height: 200.0,
    fit: BoxFit.cover,
    ),
  );
}

 Widget _crearTitulo() {
    return SafeArea(
      child: Container(
              padding: EdgeInsets.symmetric(horizontal:30.0,vertical: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Rick And Morty', style:estilortitulo,),
                        SizedBox(height: 7.0,),
                        Text('Un loco cientifico')
                      ],
                    ),
                  ),
                  Icon(Icons.star, color:Colors.red,size: 30.0,),
                  Text('41',style: TextStyle(fontSize: 20.0,)),
                ],
              ),
            ),
    );
  }

 Widget _crearAcciones() {
   return Row(
     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
     children: <Widget>[
      _accion('Call',Icons.call),
      _accion('Route',Icons.near_me),
      _accion('Share',Icons.share),
     ],
   );
 }

 Widget _accion(String text, IconData icon) {
   return  Column(
         children: <Widget>[
           Icon(icon, color: Colors.blue,size: 40.0,),
           SizedBox(height: 7.0,),
           Text(text,style: TextStyle(fontSize: 15.0,color: Colors.blue),),
         ],
       );
 }

 Widget _crarTexto() {
   return SafeArea(
     child: Container(
      padding: EdgeInsets.symmetric(horizontal: 40.0,vertical: 0.0),
       child: Text('Después de haber estado desaparecido durante casi 20 años, Rick Sánchez llega de imprevisto a la puerta de la casa de su hija Beth y se va a vivir con ella y su familia utilizando el garaje como su laboratorio personal.',
       textAlign: TextAlign.justify,
       ),
       ),
   );
 }
  


}