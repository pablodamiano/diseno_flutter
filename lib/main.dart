import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:futter_disenio_pabo/src/pages/basico_page.dart';
import 'package:futter_disenio_pabo/src/pages/botones_page.dart';
import 'package:futter_disenio_pabo/src/pages/scroll_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
     statusBarColor: (Platform.isAndroid)?Colors.transparent:Colors.white,
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Diseño',
      initialRoute: 'botones',
      routes:{
        'basico' : (BuildContext context)=> BasicoPage(),
        'scroll' : (BuildContext context)=> ScrollPage(),
        'botones' : (BuildContext context)=> BotonesPage(),
      },
    );
  }
}